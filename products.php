<?php
include('./smart_resize_image.function.php');

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

if (isset($_POST['submitBtn'])) {
    $filename = reArrayFiles($_FILES['product']);
    echo '<pre>';
    //print_r($_FILES);

    if (!empty($filename)) {
        foreach ($filename as $file) {
            move_uploaded_file($file['tmp_name'], 'uploads/large_' . $file['name']);
            $file1 = 'uploads/large_' . $file['name'];
            $resizedFile = 'uploads/small_' . $file['name'];
            smart_resize_image(null, file_get_contents($file1), 200, 200, false, $resizedFile, false, false, 100);
        }
    }
}
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>jQuery UI Draggable - Default functionality</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    </head>
    <body>
        <form action="" name="upload" method="post" enctype="multipart/form-data">
            <div class="upload">
                <input type="file" name="product[]" id="file_1">
                <button id="addmore">Add More</button>
            </div>


            <br>
            <input type="submit" value="Upload" name="submitBtn">
        </form>
    </body>

    <script>
        $(document).ready(function(e) {
            $('#addmore').on('click', function(event) {
                event.preventDefault();
                var counter = 1;
                var row = '<br><input type="file" name="product[]" id="file_' + counter + '">';
                $('#file_' + counter).after(row);
                counter++;
            })
        })
    </script>
</html>